#include <SDL.h>
#include <spdlog/spdlog.h>

#include <exception>
#include <functional>
#include <memory>
#include <string_view>
#include <utility>

template<typename... ResourceTypes>
constexpr auto with(ResourceTypes &&... resources) {
    return
        [&resources...](
            std::function<auto(ResourceTypes && ...)->void> callback) -> void {
            callback(std::forward<ResourceTypes>(resources)...);
        };
}

namespace SDL {
class Error : public std::runtime_error {
public:
    Error()
    : Error(SDL_GetError()){};
    explicit Error(std::string_view const identifier)
    : Error(identifier, SDL_GetError()){};

protected:
    Error(std::string_view const identifier, std::string_view const message)
    : std::runtime_error(fmt::format("{} {}", identifier, message)){};
};

class Init {
public:
    Init() {
        if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
            throw Error("SDL::Init");
        }
        spdlog::info("SDL::Init done.");
    }
    ~Init() {
        SDL_Quit();
        spdlog::info("SDL::Quit done.");
    }
};
} // namespace SDL

struct Resource {
    Resource() { /* set up */
        spdlog::info("Resource {}", static_cast<void *>(this));
    }
    ~Resource() { /* tear down */
        spdlog::info("~Resource {}", static_cast<void *>(this));
    }
};

auto main() -> int {
    try {
        with(SDL::Init())([](SDL::Init const && init) -> void {
            spdlog::info("after init of {}", static_cast<void const *>(&init));
        });

        with(SDL::Init(), Resource(), Resource(), Resource())(
            [](auto const &&...) { spdlog::info("after init"); });
    } catch (SDL::Error const & error) {
        spdlog::error(error.what());
    }
    return 0;
}
