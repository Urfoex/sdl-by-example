add_executable(
  run
  main.cpp
)

target_link_libraries(
  run
  SDL2::SDL2
  spdlog::spdlog
)
