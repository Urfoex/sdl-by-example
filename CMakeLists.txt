cmake_minimum_required(VERSION 3.22.1)

cmake_policy(SET CMP0048 NEW)
project(
  SDL-BY-EXAMPLE
  VERSION 0.0.0.0
  DESCRIPTION "We can do it!"
  LANGUAGES CXX
)

# SET( CMAKE_EXPORT_COMPILE_COMMANDS ON )
# SET( CMAKE_BUILD_TYPE Debug )

# Needed packages
find_package(fmt REQUIRED)
find_package(spdlog REQUIRED)
find_package(sdl2 REQUIRED)

set(
  COMPILE_OPTIONS_DEBUG
  -Wall
  -Wextra
  -pedantic
  -O0
  -g3
  -ggdb3
  # -save-temps
)
set(
  COMPILE_OPTIONS_RELEASE
  -O2
  -march=native
  -mtune=native
  # -save-temps
)

add_compile_options(
  -std=c++20
  -fdiagnostics-color=always
  "$<$<CONFIG:Debug>:${COMPILE_OPTIONS_DEBUG}>"
  "$<$<NOT:$<CONFIG:Debug>>:${COMPILE_OPTIONS_RELEASE}>"
)

add_subdirectory(examples bin/examples)
